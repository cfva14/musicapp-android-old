package io.github.cfva14.musicapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.model.Artist;
import io.github.cfva14.musicapp.utils.FirebaseHelper;

/**
 * Created by Carlos on 3/22/17.
 */

public class ArtistAdapter extends ArrayAdapter<Artist> {
    boolean isFav = false;

    private static final String TAG = ArtistAdapter.class.getSimpleName();

    public ArtistAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.gridview_tile_item, parent, false);
        }
        final Artist currentArtist = getItem(position);

        ImageView imageView = (ImageView) listItemView.findViewById(R.id.gridview_tile_imageview);
        Picasso.with(getContext()).load(currentArtist.getSource()).into(imageView);

        TextView primaryTextView = (TextView) listItemView.findViewById(R.id.gridview_tile_primary_textview);
        primaryTextView.setText(currentArtist.getName());
        primaryTextView.setSelected(true);

        TextView secondaryTextView = (TextView) listItemView.findViewById(R.id.gridview_tile_secondary_textview);
        secondaryTextView.setText(currentArtist.getGenre().toString());
        secondaryTextView.setSelected(true);

        final ImageButton favoriteButton = (ImageButton) listItemView.findViewById(R.id.gridview_tile_favorite_imagebutton);
        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isFav){
                    new FirebaseHelper().removeFav("AAA", currentArtist.getId());
                    favoriteButton.setBackgroundResource(R.drawable.ic_favorite_border);
                    isFav = false;
                } else {
                    new FirebaseHelper().setFav("AAA", currentArtist.getId());
                    favoriteButton.setBackgroundResource(R.drawable.ic_favorite_full);
                    isFav = true;
                }

            }
        });

        return listItemView;
    }

}
