package io.github.cfva14.musicapp.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Carlos on 3/26/17.
 */
@IgnoreExtraProperties
public class Playlist {

    public String uid;
    public String name;
    public String description;
    public String user_id;
    public int favoriteCount = 0;
    public Map<String, Boolean> favorites = new HashMap<>();

    public Playlist() {}

    public Playlist(String uid, String name, String description, String user_id) {
        this.uid = uid;
        this.name = name;
        this.description = description;
        this.user_id = user_id;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("name", name);
        result.put("description", description);
        result.put("user_id", user_id);
        result.put("favoriteCount", favoriteCount);
        result.put("favorites", favorites);

        return result;
    }
}
