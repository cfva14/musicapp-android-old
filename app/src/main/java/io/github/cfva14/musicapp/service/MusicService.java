package io.github.cfva14.musicapp.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import io.github.cfva14.musicapp.model.Track;

/**
 * Created by Carlos on 3/20/17.
 */

public class MusicService extends Service implements OnAudioFocusChangeListener, OnCompletionListener, OnErrorListener, OnPreparedListener{

    private final static String TAG = MusicService.class.getSimpleName();
    // The volume we set the media player to when we lose audio focus, but are
    // allowed to reduce the volume instead of stopping playback.
    public static final float VOLUME_DUCK = 0.2f;
    // The volume we set the media player when we have audio focus.
    public static final float VOLUME_NORMAL = 1.0f;

    // we don't have audio focus, and can't duck (play at a low volume)
    private static final int AUDIO_NO_FOCUS_NO_DUCK = 0;
    // we don't have focus, but can duck (play at a low volume)
    private static final int AUDIO_NO_FOCUS_CAN_DUCK = 1;
    // we have full audio focus
    private static final int AUDIO_FOCUSED  = 2;

    private static final String PLAYBACK_STATE = "io.github.cfva14.musicapp.action.PLAYBACK_STATE";

    // States of the mediaplayer
    private int state = 0;
    private static final int STATE_NULL = 0;
    private static final int STATE_BUFFERING = 1;
    private static final int STATE_PLAYING = 2;
    private static final int STATE_PAUSED = 3;
    private static final int STATE_STOPPED = 4;


    // Variables to set and check repeat and shuffle modes
    public boolean isShuffle = false;
    public boolean isRepeatOne = false;
    public boolean isRepeatAll = false;

    // Variable to hold cuurent playlist and current track
    private ArrayList<Track> tracks;
    private Track track;
    private int currentTrack = 0;

    private int audioFocus = AUDIO_NO_FOCUS_NO_DUCK;
    private boolean playOnFocusGain;
    private volatile int currentPosition;
    private volatile boolean audioNoisyReceiverRegistered;

    // Playback and audioManager Variables
    private AudioManager audioManager;
    private MediaPlayer mediaPlayer;

    private Context context;
    private WifiManager.WifiLock wifiLock;

    // Variable needed to bind service with UI
    private final IBinder musicBinder = new MusicBinder();

    private final IntentFilter audioNoisyIntentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);

    private final BroadcastReceiver audioNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                Log.e(TAG, "Headphones disconnected");
                if (state == STATE_PLAYING) {
                    stop();
                }
            }
        }
    };

    private static final long PROGRESS_UPDATE_INTERNAL = 1000;
    private static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;
    private final Handler handler = new Handler();
    private final Runnable updateProgressTask = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> mScheduleFuture;

    @Override
    public void onCreate() {
        super.onCreate();

        scheduleSeekbarUpdate();

        // Init variables neede for mediaPlayer and some other functions
        tracks = new ArrayList<>();
        context = this;
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        wifiLock = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).createWifiLock(WifiManager.WIFI_MODE_FULL, "MusicApp WifiLock");
        state = STATE_NULL;

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    // Methods needed for Binding Service ##########################################################
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return musicBinder;
    }

    public class MusicBinder extends Binder {
        public MusicService getService() {
            // Return this instance of LocalService so clients can call public methods
            return MusicService.this;
        }
    }

    // Implements Methods ##########################################################################

    @Override
    public void onAudioFocusChange(int focusChange) {

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        currentTrack += 1;
        track = tracks.get(currentTrack);
        play();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.e(TAG, "onPrepared");
        configMediaPlayerState();
        state = STATE_PLAYING;
        Intent intent = new Intent();
        intent.setAction(PLAYBACK_STATE);
        intent.putExtra("PLAYBACK_STATE", state);
        sendBroadcast(intent);

        if (track.getSource().startsWith("http")){
            boolean download = true;
            if (download){
                //Log.e(TAG, "DESCARGAR");
                //new DownloadTrack(this).execute(track);
            }
        }
    }

    // Private Methods #############################################################################

    private void configMediaPlayerState() {

        if (audioFocus == AUDIO_NO_FOCUS_NO_DUCK) {
            // If we don't have audio focus and can't duck, we have to pause,
            if (state == STATE_PLAYING) {
                pause();
            }
        } else {  // we have audio focus:
            registerAudioNoisyReceiver();
            if (audioFocus == AUDIO_NO_FOCUS_CAN_DUCK) {
                mediaPlayer.setVolume(VOLUME_DUCK, VOLUME_DUCK); // we'll be relatively quiet
            } else {
                if (mediaPlayer != null) {
                    mediaPlayer.setVolume(VOLUME_NORMAL, VOLUME_NORMAL); // we can be loud again
                } // else do something for remote client.
            }
            // If we were playing when we lost focus, we need to resume playing.
            if (playOnFocusGain) {
                if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
                    if (currentPosition == mediaPlayer.getCurrentPosition()) {
                        mediaPlayer.start();
                        state = STATE_PLAYING;
                    } else {
                        mediaPlayer.seekTo(currentPosition);
                        state = STATE_BUFFERING;
                    }
                }
                playOnFocusGain = false;
            }
        }

    }

    private void createMediaPlayerIfNeeded() {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setWakeMode(this, PowerManager.PARTIAL_WAKE_LOCK);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnErrorListener(this);
        } else {
            mediaPlayer.reset();
        }
    }

    private void relaxResources(boolean releaseMediaPlayer) {

        if (releaseMediaPlayer && mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }

        if (wifiLock.isHeld()) {
            wifiLock.release();
        }
    }

    private void tryToGetAudioFocus() {

        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            audioFocus = AUDIO_FOCUSED;
        } else {
            audioFocus = AUDIO_NO_FOCUS_NO_DUCK;
        }
    }

    private void giveUpAudioFocus() {

        if (audioManager.abandonAudioFocus(this) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            audioFocus = AUDIO_NO_FOCUS_NO_DUCK;
        }
    }

    private void registerAudioNoisyReceiver() {
        if (!audioNoisyReceiverRegistered) {
            context.registerReceiver(audioNoisyReceiver, audioNoisyIntentFilter);
            audioNoisyReceiverRegistered = true;
        }
    }

    private void unregisterAudioNoisyReceiver() {
        if (audioNoisyReceiverRegistered) {
            context.unregisterReceiver(audioNoisyReceiver);
            audioNoisyReceiverRegistered = false;
        }
    }
    // Public Methods ##############################################################################

    public int getState(){
        return state;
    }

    public void play() {
        playOnFocusGain = true;
        tryToGetAudioFocus();
        registerAudioNoisyReceiver();
        track = tracks.get(currentTrack);

        if (state == STATE_PAUSED && mediaPlayer != null) {
            configMediaPlayerState();
        } else {
            state = STATE_STOPPED;
            relaxResources(false); // release everything except MediaPlayer
            // track = tracks.get(currentTrack);

            //noinspection ResourceType
            String source = track.getSource();
            if (source != null) {
                source = source.replaceAll(" ", "%20"); // Escape spaces for URLs
            }

            try {
                createMediaPlayerIfNeeded();

                state = STATE_BUFFERING;

                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(source);

                // Starts preparing the media player in the background. When
                // it's done, it will call our OnPreparedListener (that is,
                // the onPrepared() method on this class, since we set the
                // listener to 'this'). Until the media player is prepared,
                // we *cannot* call start() on it!
                mediaPlayer.prepareAsync();

                // If we are streaming from the internet, we want to hold a
                // Wifi lock, which prevents the Wifi radio from going to
                // sleep while the song is playing.
                wifiLock.acquire();

            } catch (IOException ex) {
                Log.e(TAG, "Exception playing song");

            }
        }
    }



    public void pause(){
        if (state == STATE_PLAYING) {
            // Pause media player and cancel the 'foreground service' state.
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                currentPosition = mediaPlayer.getCurrentPosition();
            }
            // while paused, retain the MediaPlayer but give up audio focus
            relaxResources(false);
        }
        state = STATE_PAUSED;

        unregisterAudioNoisyReceiver();
    }

    public void stop(){
        currentPosition = getCurrentPosition();
        // Give up Audio focus
        giveUpAudioFocus();
        unregisterAudioNoisyReceiver();
        // Relax all resources
        relaxResources(true);
    }

    public void nextTrack(){
        Log.e(TAG, "nextTrack " + currentTrack);
        Log.e(TAG, "nextTrack");
        if(currentTrack < (tracks.size() - 1)){
            currentTrack += 1;
            Log.e(TAG, "nextTrack " + currentTrack);
        }else{
            // play first song
            currentTrack = 0;
        }
        mediaPlayer.start();
        play();

    }

    public void previousTrack(){
        Log.e(TAG, "previousTrack");
        if(currentTrack > 0){
            currentTrack -= 1;
        }else{
            // play last song
            currentTrack = tracks.size() - 1;
        }
        mediaPlayer.start();
        play();
    }

    public int getCurrentPosition(){
        return mediaPlayer.getCurrentPosition();
    }

    public void seekTo(int progress){
        Log.e(TAG, progress + "");
        mediaPlayer.seekTo(progress);
    }

    public int getTotalDuration(){
        return mediaPlayer.getDuration();
    }

    public int setRepeat(){
        return 0;
    }

    public boolean setShuffle(){
        return false;
    }

    public String getTrackTitle(){
        return track.getTitle();
    }

    public String getAlbumName(){
        return "";
    }

    public String getAlbumImage(){
        return track.getAlbum_source();
    }

    public String getArtistName(){
        return track.getArtist_name();
    }

    public String getArtistImage(){
        return track.getArtist_source();
    }

    public void setPlaylist(ArrayList<Track> tracks, int currentTrack){
        this.tracks = tracks;
        this.track = tracks.get(currentTrack);
        this.currentTrack = currentTrack;

        play();
    }

    public void addTrack(){

    }

    private void updateProgress(){
        Intent intent = new Intent();
        intent.setAction(PLAYBACK_STATE);
        intent.putExtra("PLAYBACK_STATE", state);
        //sendBroadcast(intent);
    }

    private void scheduleSeekbarUpdate() {

        if (!executorService.isShutdown()) {
            mScheduleFuture = executorService.scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            handler.post(updateProgressTask);
                        }
                    }, PROGRESS_UPDATE_INITIAL_INTERVAL,
                    PROGRESS_UPDATE_INTERNAL, TimeUnit.MILLISECONDS);
        }
    }

}
