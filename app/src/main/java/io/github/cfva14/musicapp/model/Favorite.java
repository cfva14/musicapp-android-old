package io.github.cfva14.musicapp.model;

import java.util.List;

/**
 * Created by Carlos on 3/22/17.
 */

public class Favorite {

    private String id;
    private String user_id;
    private String track_id;

    public Favorite(){}

    public Favorite(String id, String user_id, String track_id) {
        this.id = id;
        this.user_id = user_id;
        this.track_id = track_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }
}
