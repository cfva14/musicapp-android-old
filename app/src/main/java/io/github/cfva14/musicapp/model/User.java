package io.github.cfva14.musicapp.model;

/**
 * Created by Carlos on 3/26/17.
 */

public class User {

    private String username;
    private String email;

    public User(){}

    public User(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
