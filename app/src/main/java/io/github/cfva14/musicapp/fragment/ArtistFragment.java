package io.github.cfva14.musicapp.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.cfva14.musicapp.Playback;
import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.activity.AlbumActivity;
import io.github.cfva14.musicapp.adapter.AlbumAdapter;
import io.github.cfva14.musicapp.adapter.PlaylistAdapter;
import io.github.cfva14.musicapp.adapter.TrackAdapter;
import io.github.cfva14.musicapp.custom.ExpandedHeigthGridView;
import io.github.cfva14.musicapp.custom.ExpandedHeigthListView;
import io.github.cfva14.musicapp.model.Album;
import io.github.cfva14.musicapp.model.Playlist;
import io.github.cfva14.musicapp.model.Search;
import io.github.cfva14.musicapp.model.Track;


/**
 * Created by Carlos on 3/13/17.
 */

public class ArtistFragment extends Fragment {

    private static final String TAG = ArtistFragment.class.getSimpleName();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseAuth firebaseAuth;
    DatabaseReference databaseReference;
    DatabaseReference trackRef = database.getReference("track");
    DatabaseReference userRef = database.getReference("users");
    DatabaseReference albumRef = database.getReference("album");
    DatabaseReference playlistRef = database.getReference("playlist");

    private TrackAdapter trackAdapter;
    private AlbumAdapter albumAdapter;
    private PlaylistAdapter playlistAdapter;

    private ArrayList<Track> tracks;
    private ArrayList<Album> albums;
    private ArrayList<Playlist> playlists;

    private Track currentTrack;

    private String userUid;

    // interface to controls playback
    private Playback playback;

    // Get elements from UI
    @BindView(R.id.fragment_artist_cover_imageview) ImageView coverImageView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_artist, container, false);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        userUid = firebaseAuth.getCurrentUser().getUid();

        // Bind elements from UI
        ButterKnife.bind(this, rootView);

        // Init interface playback
        playback = (Playback) getActivity();

        String artistId = getArguments().getString("ID");
        String artistSource = getArguments().getString("SOURCE");

        Picasso.with(getActivity()).load(artistSource).into(coverImageView);

        ExpandedHeigthListView trackListView = (ExpandedHeigthListView) rootView.findViewById(R.id.artist_listview);
        ExpandedHeigthGridView albumGridview = (ExpandedHeigthGridView) rootView.findViewById(R.id.artist_gridview);

        albumAdapter = new AlbumAdapter(getActivity());
        trackAdapter = new TrackAdapter(getActivity());
        playlistAdapter = new PlaylistAdapter(getActivity());

        tracks = new ArrayList<>();
        albums = new ArrayList<>();
        playlists = new ArrayList<>();

        Query trackQuery = trackRef.orderByChild("artist_id").equalTo(artistId).limitToFirst(5);
        Query albumQuery = albumRef.orderByChild("artist_id").equalTo(artistId);

        trackQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Track track = child.getValue(Track.class);
                    trackAdapter.add(track);
                    tracks.add(track);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        albumQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Album album = child.getValue(Album.class);
                    albumAdapter.add(album);
                    albums.add(album);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });

        trackListView.setAdapter(trackAdapter);
        trackListView.setExpanded(true);
        registerForContextMenu(trackListView);
        trackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                playback.setPlaylist(tracks, position);
            }
        });

        albumGridview.setAdapter(albumAdapter);
        albumGridview.setExpanded(true);
        albumGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG, "CLICK");
                Intent intent = new Intent(getActivity(), AlbumActivity.class);
                intent.putExtra("ID", albums.get(position).getId());
                intent.putExtra("NAME", albums.get(position).getName());
                intent.putExtra("SOURCE", albums.get(position).getSource());
                startActivity(intent);
            }
        });
        return rootView;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.track_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;
        currentTrack = tracks.get(position);

        switch (item.getItemId()){
            case R.id.track_menu_play_next:
                Log.e(TAG, "PLAY");
                break;
            case R.id.track_menu_add_queue:
                Log.e(TAG, "QUEUE");
                break;
            case R.id.track_menu_add_playlist:
                Log.e(TAG, "PLAYLIST");
                addToPlaylist();
                break;
            case R.id.track_menu_go_album:
                Log.e(TAG, "ALBUM");
                break;
            case R.id.track_menu_go_artist:
                Log.e(TAG, "ARTIST");
                break;
            case R.id.track_menu_download:
                Log.e(TAG, "DOWNLOAD");
                break;
        }
        return super.onContextItemSelected(item);
    }

    private void addToPlaylist(){
        Log.e(TAG, "createNewPlaylist");

        // Clear lists before adding new elements
        playlists.clear();
        playlistAdapter.clear();

        Query playlistQuery = databaseReference.child("user-playlists").child(userUid);
        playlistQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Playlist playlist = child.getValue(Playlist.class);
                    playlists.add(playlist);
                    playlistAdapter.add(playlist);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add to Playlist");

        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("New Playlist", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                createNewPlaylist();
            }
        });

        builder.setAdapter(playlistAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlertDialog.Builder builderInner = new AlertDialog.Builder(getActivity());


                databaseReference.child("playlist-tracks").child(userUid).child(playlistAdapter.getItem(which).uid).push().setValue(currentTrack);


                builderInner.setMessage(currentTrack.getTitle() + " ha sido agregado a la lista de reproduccion exitosamente");
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });

        builder.show();


    }

    private void createNewPlaylist(){

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setLayoutParams(params);
        layout.setPadding(100, 40, 100, 40);

        final EditText namePlaylist = new EditText(getActivity());
        namePlaylist.setHint("Name");
        layout.addView(namePlaylist);

        final EditText descriptionPlaylist = new EditText(getActivity());
        descriptionPlaylist.setHint("Description");
        layout.addView(descriptionPlaylist);

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("New Playlist");

        builderSingle.setView(layout);

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setPositiveButton("create playlist", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = namePlaylist.getText().toString();
                String description = descriptionPlaylist.getText().toString();

                String key = databaseReference.child("playlists").push().getKey();
                Playlist playlist = new Playlist(key, name, description, userUid);
                Map<String, Object> values = playlist.toMap();

                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("/playlists/" + key, values);
                childUpdates.put("/user-playlists/" + userUid + "/" + key, values);

                databaseReference.updateChildren(childUpdates);

                databaseReference.child("/playlist-tracks/" + userUid + "/" + key).push().setValue(currentTrack);


            }
        });

        final AlertDialog dialog = builderSingle.show();
        dialog.show();

        final Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        positiveButtonLL.gravity = Gravity.CENTER;
        positiveButton.setLayoutParams(positiveButtonLL);

    }
}
