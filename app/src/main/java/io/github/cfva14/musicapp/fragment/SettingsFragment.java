package io.github.cfva14.musicapp.fragment;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.util.Log;;import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.activity.OnlineActivity;

/**
 * Created by Carlos on 3/20/17.
 */

public class SettingsFragment extends PreferenceFragmentCompat implements OnSharedPreferenceChangeListener{

    // Logging TAG
    private static final String TAG = OnlineActivity.class.getSimpleName();

    // Shared Preferences Keys
    public static final String OFFLINE_ONLY = "pref_offline_only";
    public static final String DOWNLOAD_ON_WIFI_ONLY = "pref_download_on_wifi_only";

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.app_preferences);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(DOWNLOAD_ON_WIFI_ONLY)){
            Log.e("PREFERENCES", "OFFLINE");

        }

        if (key.equals(OFFLINE_ONLY)){
            Log.e("PREFERENCES", "OFFLINE");
        }
    }

}
