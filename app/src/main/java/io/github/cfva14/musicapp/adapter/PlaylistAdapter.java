package io.github.cfva14.musicapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.model.Playlist;

/**
 * Created by Carlos on 3/26/17.
 */

public class PlaylistAdapter extends ArrayAdapter<Playlist> {

    private static final String TAG = PlaylistAdapter.class.getSimpleName();

    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    DatabaseReference databaseReference;
    private String userUid;

    private ImageButton favoriteButton;

    private Playlist currentPlaylist;

    boolean isFavorite;

    public PlaylistAdapter(Context context) {
        super(context, 0);

        userUid = firebaseAuth.getCurrentUser().getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.listview_playlist_item, parent, false);
        }
        currentPlaylist = getItem(position);

        TextView nameTextView = (TextView) listItemView.findViewById(R.id.playlist_listview_name);
        nameTextView.setText(currentPlaylist.name);

        TextView descriptionTextView = (TextView) listItemView.findViewById(R.id.playlist_listview_description);
        descriptionTextView.setText(currentPlaylist.description);

        favoriteButton = (ImageButton) listItemView.findViewById(R.id.playlist_listview_favorite_imagebutton);
        favoriteButton.setFocusable(false);
        favoriteButton.setFocusableInTouchMode(false);
        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e(TAG, "CLICK");
                final String playlistRef = currentPlaylist.uid;
                DatabaseReference globalPlaylistRef = databaseReference.child("playlists").child(playlistRef);
                DatabaseReference userPlaylistRef = databaseReference.child("user-playlists").child(userUid).child(playlistRef);



                if (onFavoriteClicked(userPlaylistRef)){
                    favoriteButton.setBackgroundResource(R.drawable.ic_favorite_full);
                } else {
                    favoriteButton.setBackgroundResource(R.drawable.ic_favorite_border);
                }
                onFavoriteClicked(globalPlaylistRef);
            }
        });

        if (currentPlaylist.favorites.containsKey(userUid)){
            favoriteButton.setBackgroundResource(R.drawable.ic_favorite_full);
        } else {
            favoriteButton.setBackgroundResource(R.drawable.ic_favorite_border);
        }

        return listItemView;
    }

    private boolean onFavoriteClicked(DatabaseReference playlistRef) {
        playlistRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Playlist playlist = mutableData.getValue(Playlist.class);
                if (playlist == null) {
                    return Transaction.success(mutableData);
                }

                if (playlist.favorites.containsKey(userUid)) {

                    // Unstar the post and remove self from stars
                    playlist.favoriteCount = playlist.favoriteCount - 1;
                    playlist.favorites.remove(userUid);
                    isFavorite = false;
                } else {

                    // Star the post and add self to stars
                    playlist.favoriteCount = playlist.favoriteCount + 1;
                    playlist.favorites.put(userUid, true);
                    isFavorite = true;
                }

                // Set value and report transaction success
                mutableData.setValue(playlist);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                // Transaction completed
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });

        return isFavorite;
    }

}
