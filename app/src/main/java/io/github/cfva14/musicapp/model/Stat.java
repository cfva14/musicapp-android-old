package io.github.cfva14.musicapp.model;

/**
 * Created by Carlos on 3/22/17.
 */

public class Stat {

    private int listeners;
    private int plays;
    private int fans;
    private int tracks;

    public Stat(){}

    public Stat(int listeners, int plays, int fans, int tracks) {
        this.listeners = listeners;
        this.plays = plays;
        this.fans = fans;
        this.tracks = tracks;
    }

    public int getListeners() {
        return listeners;
    }

    public void setListeners(int listeners) {
        this.listeners = listeners;
    }

    public int getPlays() {
        return plays;
    }

    public void setPlays(int plays) {
        this.plays = plays;
    }

    public int getFans() {
        return fans;
    }

    public void setFans(int fans) {
        this.fans = fans;
    }

    public int getTracks() {
        return tracks;
    }

    public void setTracks(int tracks) {
        this.tracks = tracks;
    }
}
