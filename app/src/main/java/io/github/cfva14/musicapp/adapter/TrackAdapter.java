package io.github.cfva14.musicapp.adapter;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.model.Track;

/**
 * Created by Carlos on 3/21/17.
 */

public class TrackAdapter extends ArrayAdapter<Track>{

    private static final String TAG = TrackAdapter.class.getSimpleName();

    public TrackAdapter(Context context){
        super(context, 0);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Check if an existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.listview_track_item, parent, false);
        }

        Track currentTrack = getItem(position);

        TextView number = (TextView) listItemView.findViewById(R.id.listview_track_number);
        number.setText(currentTrack.getTrack_number() + "");

        TextView title = (TextView) listItemView.findViewById(R.id.listview_track_title);
        title.setText(currentTrack.getTitle());

        return listItemView;
    }




}
