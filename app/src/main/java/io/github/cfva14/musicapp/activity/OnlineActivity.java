package io.github.cfva14.musicapp.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.fragment.BrowseFragment;
import io.github.cfva14.musicapp.fragment.ChartFragment;
import io.github.cfva14.musicapp.fragment.ControlsFragment;
import io.github.cfva14.musicapp.fragment.DiscoverFragment;
import io.github.cfva14.musicapp.fragment.PlaylistFragment;
import io.github.cfva14.musicapp.fragment.SearchFragment;
import io.github.cfva14.musicapp.fragment.SettingsFragment;
import io.github.cfva14.musicapp.service.MusicService;

public class OnlineActivity extends BaseActivity {

    // Logging TAG
    private static final String TAG = OnlineActivity.class.getSimpleName();

    private static final String PLAYBACK_STATE = "io.github.cfva14.musicapp.action.PLAYBACK_STATE";

    // Fragment TAGS
    private static final String DISCOVER_FRAGMENT_TAG = "discover_fragment_tag";
    private static final String BROWSE_FRAGMENT_TAG = "browser_fragment_tag";
    private static final String CHART_FRAGMENT_TAG = "chart_fragment_tag";
    private static final String SEARCH_FRAGMENT_TAG = "search_fragment_tag";
    private static final String PLAYLIST_FRAGMENT_TAG = "playlist_fragment_tag";
    private static final String SETTINGS_FRAGMENT_TAG = "settings_fragment_tag";
    private static final String CONTROLS_FRAGMENT_TAG = "control_fragment_tag";

    // States of the mediaplayer
    private int state = 0;
    private static final int STATE_NULL = 0;
    private static final int STATE_BUFFERING = 1;
    private static final int STATE_PLAYING = 2;
    private static final int STATE_PAUSED = 3;
    private static final int STATE_STOPPED = 4;

    // Actions to determinate which fragment have to be shown
    private static final int ACTION_OPEN_DISCOVER_FRAGMENT = 0;
    private static final int ACTION_OPEN_BROWSE_FRAGMENT = 1;
    private static final int ACTION_OPEN_CHART_FRAGMENT = 2;
    private static final int ACTION_OPEN_SEARCH_FRAGMENT = 3;
    private static final int ACTION_OPEN_PLAYLIST_FRAGMENT = 4;
    private static final int ACTION_OPEN_SETTINGS_FRAGMENT = 5;

    public SlidingUpPanelLayout slidingUpPanelLayout;

    private IntentFilter intentFilter;
    private BroadcastReceiver musicStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getIntExtra("PLAYBACK_STATE", 0) == 2){
                Log.e(TAG, "PLAYING");
                // If slidingPanel is hidden Show when Start Playing
                if (slidingUpPanelLayout.getPanelState() == PanelState.HIDDEN){
                    slidingUpPanelLayout.setPanelState(PanelState.COLLAPSED);
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online);

        // Init toolbar
        initializeToolbar();

        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        intentFilter = new IntentFilter("io.github.cfva14.musicapp.action.PLAYBACK_STATE");
        registerReceiver(musicStateReceiver, intentFilter);

        // Decide which fragment have to show according to the ACTION
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        int action_fragment = getIntent().getIntExtra("FRAGMENT", 0);

        switch (action_fragment) {
            case ACTION_OPEN_DISCOVER_FRAGMENT:
                DiscoverFragment discoverFragment = new DiscoverFragment();
                fragmentTransaction.add(R.id.fragment_container, discoverFragment, DISCOVER_FRAGMENT_TAG);
                fragmentTransaction.commit();
                break;

            case ACTION_OPEN_BROWSE_FRAGMENT:
                BrowseFragment browseFragment = new BrowseFragment();
                fragmentTransaction.add(R.id.fragment_container, browseFragment, BROWSE_FRAGMENT_TAG);
                fragmentTransaction.commit();
                break;

            case ACTION_OPEN_CHART_FRAGMENT:
                ChartFragment chartFragment = new ChartFragment();
                fragmentTransaction.add(R.id.fragment_container, chartFragment, CHART_FRAGMENT_TAG);
                fragmentTransaction.commit();
                break;

            case ACTION_OPEN_SEARCH_FRAGMENT:
                SearchFragment searchFragment = new SearchFragment();
                fragmentTransaction.add(R.id.fragment_container, searchFragment, SEARCH_FRAGMENT_TAG);
                fragmentTransaction.commit();
                break;

            case ACTION_OPEN_PLAYLIST_FRAGMENT:
                PlaylistFragment playlistFragment = new PlaylistFragment();
                fragmentTransaction.add(R.id.fragment_container, playlistFragment, PLAYLIST_FRAGMENT_TAG);
                fragmentTransaction.commit();
                break;

            case ACTION_OPEN_SETTINGS_FRAGMENT:
                SettingsFragment settingsFragment = new SettingsFragment();
                fragmentTransaction.add(R.id.fragment_container, settingsFragment, SETTINGS_FRAGMENT_TAG);
                fragmentTransaction.commit();
                break;

        }

        // Open control fragment
        ControlsFragment controlsFragment = new ControlsFragment();
        FragmentTransaction fragmentTransactionControls = getSupportFragmentManager().beginTransaction();
        fragmentTransactionControls.add(R.id.controls_fragment_container, controlsFragment, CONTROLS_FRAGMENT_TAG);
        fragmentTransactionControls.commit();

        // Check the status and show sliding Panel if playing, paused, buffering
        if (bound){
            if (musicService.getState() == STATE_PLAYING || musicService.getState() == STATE_PAUSED || musicService.getState() == STATE_BUFFERING){
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                Log.e(TAG, "SHOW");
            } else {
                Log.e(TAG, "HIDE");
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        } else {
            Log.e(TAG, "No BOUND");
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        }

        slidingUpPanelLayout.addPanelSlideListener(new PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, PanelState previousState, PanelState newState) {
                Intent intent = new Intent();
                intent.setAction(PLAYBACK_STATE);
                if (newState.equals(PanelState.COLLAPSED)){
                    intent.putExtra("PLAYBACK_STATE", 5);
                } else if(newState.equals(PanelState.EXPANDED)) {
                    intent.putExtra("PLAYBACK_STATE", 6);
                }
                sendBroadcast(intent);

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        registerReceiver(musicStateReceiver, intentFilter);
        isOnlineActivityRunning = true;
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(musicStateReceiver);
        isOnlineActivityRunning = false;

    }


}
