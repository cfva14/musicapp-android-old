package io.github.cfva14.musicapp.model;

/**
 * Created by Carlos on 3/20/17.
 */

public class Track {

    private String id;
    private String title;
    private String source;
    private String display_name;
    private int duration;
    private int track_number;
    private String artist_id;
    private String artist_name;
    private String artist_source;
    private String album_id;
    private String album_name;
    private String album_source;
    private int plays;
    private int listeners;


    public Track() {
    }


    public Track(String id, String title, String source, String display_name, int duration, int track_number, String artist_id, String artist_name, String artist_source, String album_id, String album_name, String album_source, int plays, int listeners) {
        this.id = id;
        this.title = title;
        this.source = source;
        this.display_name = display_name;
        this.duration = duration;
        this.track_number = track_number;
        this.artist_id = artist_id;
        this.artist_name = artist_name;
        this.artist_source = artist_source;
        this.album_id = album_id;
        this.album_name = album_name;
        this.album_source = album_source;
        this.plays = plays;
        this.listeners = listeners;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getTrack_number() {
        return track_number;
    }

    public void setTrack_number(int track_number) {
        this.track_number = track_number;
    }

    public String getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(String artist_id) {
        this.artist_id = artist_id;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getArtist_source() {
        return artist_source;
    }

    public void setArtist_source(String artist_source) {
        this.artist_source = artist_source;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getAlbum_source() {
        return album_source;
    }

    public void setAlbum_source(String album_source) {
        this.album_source = album_source;
    }

    public int getPlays() {
        return plays;
    }

    public void setPlays(int plays) {
        this.plays = plays;
    }

    public int getListeners() {
        return listeners;
    }

    public void setListeners(int listeners) {
        this.listeners = listeners;
    }
}
