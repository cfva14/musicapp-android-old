package io.github.cfva14.musicapp.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.adapter.TrackAdapter;
import io.github.cfva14.musicapp.model.Track;
import io.github.cfva14.musicapp.service.MusicService;

public class CurrentPlaylistActivity extends AppCompatActivity {

    private static final String TAG = CurrentPlaylistActivity.class.getSimpleName();

    private DatabaseReference firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private String userUid;

    private ListView listView;
    private TextView textView;

    private ArrayList<Track> tracks;
    private TrackAdapter trackAdapter;

    public MusicService musicService;
    public boolean bound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_playlist);

        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        userUid = firebaseAuth.getCurrentUser().getUid();

        listView = (ListView) findViewById(R.id.current_playlist);
        textView = (TextView) findViewById(R.id.current_playlist_title);

        tracks = new ArrayList<>();
        trackAdapter = new TrackAdapter(this);

        String uid = getIntent().getStringExtra("UID");
        String name = getIntent().getStringExtra("NAME");

        textView.setText(name);

        Query query = firebaseDatabase.child("playlist-tracks/" + userUid + "/" + uid);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Track track = child.getValue(Track.class);

                    trackAdapter.add(track);
                    tracks.add(track);

                    Log.e(TAG, child.getValue().toString());

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        listView.setAdapter(trackAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                musicService.setPlaylist(tracks, position);
                Log.e(TAG, position + "");
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, MusicService.class);
        startService(intent);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (bound) {
            unbindService(serviceConnection);
            bound = false;
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            MusicService.MusicBinder musicBinder = (MusicService.MusicBinder) service;
            musicService = musicBinder.getService();
            bound = true;
            Log.e(TAG, "onConnected " + bound);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;

            Log.e(TAG, "onDisconnected " + bound);
        }
    };
}
