package io.github.cfva14.musicapp.model;

import java.util.List;

/**
 * Created by Carlos on 3/22/17.
 */

public class Bio {

    private String summary;
    private List<String> members = null;

    public Bio(){}

    public Bio(String summary, List<String> members) {
        this.summary = summary;
        this.members = members;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }
}
