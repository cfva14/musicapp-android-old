package io.github.cfva14.musicapp.utils;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import io.github.cfva14.musicapp.model.Artist;
import io.github.cfva14.musicapp.model.Favorite;

/**
 * Created by Carlos on 3/22/17.
 */

public class FirebaseHelper {

    boolean fav = false;
    private static final String TAG = FirebaseHelper.class.getSimpleName();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference userRef = database.getReference("user");

    public boolean getFav(String userId, final String artistId){

        Query query = userRef.orderByChild("id").equalTo(userId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    child.getRef().child("favorite").child(artistId).setValue(true);
                    if (child.hasChild("favorite/" + artistId)){
                        fav = true;
                        Log.e(TAG, fav + "");
                    }
                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        return fav;
    }

    public void setFav(String userId, final String artistId){
        Query query = userRef.orderByChild("id").equalTo(userId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    child.getRef().child("favorite").child(artistId).setValue(true);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    public void removeFav(String userId, final String artistId){
        Query query = userRef.orderByChild("id").equalTo(userId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    child.getRef().child("favorite").child(artistId).removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }




}
