package io.github.cfva14.musicapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.model.Search;

/**
 * Created by Carlos on 3/24/17.
 */

public class SearchAdapter extends ArrayAdapter<Search> {

    public SearchAdapter(Context context){
        super(context, 0);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View listViewItem = convertView;

        if (listViewItem == null){
            listViewItem = LayoutInflater.from(getContext()).inflate(R.layout.listview_search_item, parent, false);
        }

        Search currentSearch = getItem(position);

        TextView searchTextView = (TextView) listViewItem.findViewById(R.id.listview_search_item_textview);
        searchTextView.setText(currentSearch.getSearch());

        return listViewItem;
    }
}
