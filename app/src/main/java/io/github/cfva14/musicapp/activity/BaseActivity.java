package io.github.cfva14.musicapp.activity;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import io.github.cfva14.musicapp.model.Track;
import io.github.cfva14.musicapp.service.MusicService;
import io.github.cfva14.musicapp.Playback;
import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.fragment.BrowseFragment;
import io.github.cfva14.musicapp.fragment.ChartFragment;
import io.github.cfva14.musicapp.fragment.DiscoverFragment;
import io.github.cfva14.musicapp.fragment.PlaylistFragment;
import io.github.cfva14.musicapp.fragment.SearchFragment;
import io.github.cfva14.musicapp.fragment.SettingsFragment;

/**
 * Created by Carlos on 3/20/17.
 */

public class BaseActivity extends AppCompatActivity implements Playback{

    // Logging TAG
    private static final String TAG = BaseActivity.class.getSimpleName();

    private static final String PLAYBACK_STATE = "io.github.cfva14.musicapp.action.PLAYBACK_STATE";

    // Fragment TAGS
    private static final String DISCOVER_FRAGMENT_TAG = "discover_fragment_tag";
    private static final String BROWSE_FRAGMENT_TAG = "browser_fragment_tag";
    private static final String CHART_FRAGMENT_TAG = "chart_fragment_tag";
    private static final String SEARCH_FRAGMENT_TAG = "search_fragment_tag";
    private static final String PLAYLIST_FRAGMENT_TAG = "playlist_fragment_tag";
    private static final String SETTINGS_FRAGMENT_TAG = "settings_fragment_tag";

    // Actions to determinate which fragment have to be shown
    private static final int ACTION_OPEN_DISCOVER_FRAGMENT = 0;
    private static final int ACTION_OPEN_BROWSE_FRAGMENT = 1;
    private static final int ACTION_OPEN_CHART_FRAGMENT = 2;
    private static final int ACTION_OPEN_SEARCH_FRAGMENT = 3;
    private static final int ACTION_OPEN_PLAYLIST_FRAGMENT = 4;
    private static final int ACTION_OPEN_SETTINGS_FRAGMENT = 5;

    // Actions to determinate if repeat of shuffle is activated
    private static final int ACTION_REPEAT_OFF = 0;
    private static final int ACTION_REPEAT_ONE = 1;
    private static final int ACTION_REPEAT_ALL = 2;

    // Variables for toolbar and navigation drawer
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private boolean toolbarInitialized;
    private int itemToOpenWhenDrawerCloses = -1;

    // Variable to check is MainActivity is Running
    public boolean isOnlineActivityRunning =  false;

    // Variables for service
    public MusicService musicService;
    public boolean bound = false;

    private final DrawerLayout.DrawerListener mDrawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerClosed(View drawerView) {
            if (drawerToggle != null) drawerToggle.onDrawerClosed(drawerView);
            if (itemToOpenWhenDrawerCloses >= 0) {

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                Intent intent;
                switch (itemToOpenWhenDrawerCloses) {

                    case R.id.drawer_discover:
                        if (isOnlineActivityRunning){
                            DiscoverFragment discoverFragment = new DiscoverFragment();
                            fragmentTransaction.replace(R.id.fragment_container, discoverFragment, DISCOVER_FRAGMENT_TAG);
                            fragmentTransaction.commit();
                        } else {
                            intent = new Intent(BaseActivity.this, OnlineActivity.class);
                            intent.putExtra("FRAGMENT", ACTION_OPEN_DISCOVER_FRAGMENT);
                            startActivity(intent);
                            finish();
                        }

                        break;

                    case R.id.drawer_browse:
                        if (isOnlineActivityRunning){
                            BrowseFragment browserFragment = new BrowseFragment();
                            fragmentTransaction.replace(R.id.fragment_container, browserFragment, BROWSE_FRAGMENT_TAG);
                            fragmentTransaction.commit();
                        } else {
                            intent = new Intent(BaseActivity.this, OnlineActivity.class);
                            intent.putExtra("FRAGMENT", ACTION_OPEN_BROWSE_FRAGMENT);
                            startActivity(intent);
                            finish();
                        }

                        break;

                    case R.id.drawer_chart:
                        if (isOnlineActivityRunning){
                            ChartFragment chartFragment = new ChartFragment();
                            fragmentTransaction.replace(R.id.fragment_container, chartFragment, CHART_FRAGMENT_TAG);
                            fragmentTransaction.commit();
                        } else {
                            intent = new Intent(BaseActivity.this, OnlineActivity.class);
                            intent.putExtra("FRAGMENT", ACTION_OPEN_CHART_FRAGMENT);
                            startActivity(intent);
                            finish();
                        }

                        break;

                    case R.id.drawer_search:
                        if (isOnlineActivityRunning){
                            SearchFragment searchFragment = new SearchFragment();
                            fragmentTransaction.replace(R.id.fragment_container, searchFragment, SEARCH_FRAGMENT_TAG);
                            fragmentTransaction.commit();
                        } else {
                            intent = new Intent(BaseActivity.this, OnlineActivity.class);
                            intent.putExtra("FRAGMENT", ACTION_OPEN_SEARCH_FRAGMENT);
                            startActivity(intent);
                            finish();
                        }

                        break;

                    case R.id.drawer_playlist:
                        if (isOnlineActivityRunning){
                            PlaylistFragment playlistFragment = new PlaylistFragment();
                            fragmentTransaction.replace(R.id.fragment_container, playlistFragment, PLAYLIST_FRAGMENT_TAG);
                            fragmentTransaction.commit();
                        } else {
                            intent = new Intent(BaseActivity.this, OnlineActivity.class);
                            intent.putExtra("FRAGMENT", ACTION_OPEN_PLAYLIST_FRAGMENT);
                            startActivity(intent);
                            finish();
                        }

                        break;

                    case R.id.drawer_settings:
                        if (isOnlineActivityRunning){
                            SettingsFragment settingsFragment = new SettingsFragment();
                            fragmentTransaction.replace(R.id.fragment_container, settingsFragment, SETTINGS_FRAGMENT_TAG);
                            fragmentTransaction.commit();
                        } else {
                            intent = new Intent(BaseActivity.this, OnlineActivity.class);
                            intent.putExtra("FRAGMENT", ACTION_OPEN_SETTINGS_FRAGMENT);
                            startActivity(intent);
                            finish();
                        }

                        break;

                    case R.id.drawer_offline:
                        if (isOnlineActivityRunning){
                            startActivity(new Intent(getApplicationContext(), OfflineActivity.class));
                            finish();
                        }
                        break;
                }
            }
        }

        @Override
        public void onDrawerStateChanged(int newState) {
            if (drawerToggle != null) drawerToggle.onDrawerStateChanged(newState);
        }

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            if (drawerToggle != null) drawerToggle.onDrawerSlide(drawerView, slideOffset);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            if (drawerToggle != null) drawerToggle.onDrawerOpened(drawerView);
            if (getSupportActionBar() != null) getSupportActionBar()
                    .setTitle(R.string.app_name);
        }
    };

    private final FragmentManager.OnBackStackChangedListener onBackStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            updateDrawerToggle();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!toolbarInitialized) {
            throw new IllegalStateException("You must run super.initializeToolbar at " +
                    "the end of your onCreate method");
        }

        Intent intent = new Intent(this, MusicService.class);
        startService(intent);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (drawerToggle != null) {
            drawerToggle.syncState();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportFragmentManager().addOnBackStackChangedListener(onBackStackChangedListener);

    }

    @Override
    public void onPause() {
        super.onPause();

        getSupportFragmentManager().removeOnBackStackChangedListener(onBackStackChangedListener);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Unbind from the service
        if (bound) {
            unbindService(serviceConnection);
            bound = false;
        }

        Log.e(TAG, "onDestroy " + bound);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (drawerToggle != null) {
            drawerToggle.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle != null && drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // If not handled by drawerToggle, home needs to be handled by returning to previous
        if (item != null && item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // If the drawer is open, back will close it
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
            return;
        }
        // Otherwise, it may return to the previous fragment stack
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            // Lastly, it will rely on the system behavior for back
            super.onBackPressed();
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        toolbar.setTitle(title);
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);
        toolbar.setTitle(titleId);
    }

    protected void initializeToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            throw new IllegalStateException("Layout is required to include a Toolbar with id " +
                    "'toolbar'");
        }
        toolbar.inflateMenu(R.menu.main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawerLayout != null) {
            NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
            if (navigationView == null) {
                throw new IllegalStateException("Layout requires a NavigationView " +
                        "with id 'nav_view'");
            }

            // Create an ActionBarDrawerToggle that will handle opening/closing of the drawer:
            drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_content_drawer, R.string.close_content_drawer);
            drawerLayout.setDrawerListener(mDrawerListener);
            populateDrawerItems(navigationView);
            setSupportActionBar(toolbar);
            updateDrawerToggle();
        } else {
            setSupportActionBar(toolbar);
        }

        toolbarInitialized = true;
    }

    private void populateDrawerItems(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        itemToOpenWhenDrawerCloses = menuItem.getItemId();
                        drawerLayout.closeDrawers();
                        return true;
                    }
                });
        if (OnlineActivity.class.isAssignableFrom(getClass())) {
            navigationView.setCheckedItem(R.id.drawer_discover);
        } else if (OfflineActivity.class.isAssignableFrom(getClass())) {
            navigationView.setCheckedItem(R.id.drawer_offline);
        }
    }

    protected void updateDrawerToggle() {
        if (drawerToggle == null) {
            return;
        }
        boolean isRoot = getFragmentManager().getBackStackEntryCount() == 0;
        drawerToggle.setDrawerIndicatorEnabled(isRoot);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(!isRoot);
            getSupportActionBar().setDisplayHomeAsUpEnabled(!isRoot);
            getSupportActionBar().setHomeButtonEnabled(!isRoot);
        }
        if (isRoot) {
            drawerToggle.syncState();
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            MusicService.MusicBinder musicBinder = (MusicService.MusicBinder) service;
            musicService = musicBinder.getService();
            bound = true;
            Log.e(TAG, "onConnected " + bound);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;

            Log.e(TAG, "onDisconnected " + bound);
        }
    };

    // Methods from the Playback interface

    @Override
    public int getState(){
        if (bound){
            return musicService.getState();
        } else {
            return 0;
        }

    }

    @Override
    public void play(){
        musicService.play();
    }

    @Override
    public void pause(){
        musicService.pause();
    }

    @Override
    public void nextTrack(){
        musicService.nextTrack();
    }

    @Override
    public void previousTrack(){
        musicService.previousTrack();
    }

    @Override
    public int getCurrentPosition(){
        return musicService.getCurrentPosition();
    }

    @Override
    public int getTotalDuration(){
        return musicService.getTotalDuration();
    }

    @Override
    public void seekTo(int progress){
        musicService.seekTo(progress);
    }

    @Override
    public int setRepeat(){
        return 0;
    }

    @Override
    public boolean setShuffle(){
        return false;
    }

    @Override
    public String getTrackTitle(){
        return musicService.getTrackTitle();
    }

    @Override
    public String getAlbumName(){
        return musicService.getAlbumName();
    }

    @Override
    public String getAlbumImage(){
        return musicService.getAlbumImage();
    }

    @Override
    public String getArtistName(){
        return musicService.getArtistName();
    }

    @Override
    public String getArtistImage(){
        return musicService.getArtistImage();
    }

    @Override
    public void setPlaylist(ArrayList<Track> tracks, int position){
        musicService.setPlaylist(tracks, position);
    }

    @Override
    public void addTrack(){

    }

    private ProgressDialog mProgressDialog;

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}
