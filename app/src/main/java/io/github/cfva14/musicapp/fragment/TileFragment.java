package io.github.cfva14.musicapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import io.github.cfva14.musicapp.R;

/**
 * Created by Carlos on 3/20/17.
 */

public class TileFragment extends Fragment {

    private static final String TAG = DiscoverFragment.class.getSimpleName();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tile, container, false);

        String imageSource = getArguments().getString("IMAGE");
        ImageView imageView = (ImageView) rootView.findViewById(R.id.tile_imageview);
        Picasso.with(getActivity()).load(imageSource).into(imageView);

        return rootView;
    }

}
