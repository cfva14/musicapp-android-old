package io.github.cfva14.musicapp;

import java.util.ArrayList;

import io.github.cfva14.musicapp.model.Track;

/**
 * Created by Carlos on 3/20/17.
 */

public interface Playback {

    int getState();

    void play();

    void pause();

    void nextTrack();

    void previousTrack();

    int getCurrentPosition();

    int getTotalDuration();

    void seekTo(int progress);

    int setRepeat();

    boolean setShuffle();

    String getTrackTitle();

    String getAlbumName();

    String getAlbumImage();

    String getArtistName();

    String getArtistImage();

    void setPlaylist(ArrayList<Track> tracks, int position);

    void addTrack();

}
