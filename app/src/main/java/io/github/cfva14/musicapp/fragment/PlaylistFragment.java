package io.github.cfva14.musicapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import io.github.cfva14.musicapp.Playback;
import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.activity.CurrentPlaylistActivity;
import io.github.cfva14.musicapp.adapter.PlaylistAdapter;
import io.github.cfva14.musicapp.model.Playlist;

/**
 * Created by Carlos on 3/26/17.
 */

public class PlaylistFragment extends Fragment {

    private static final String TAG = PlaylistFragment.class.getSimpleName();

    // Fragment TAGS
    private static final String CURRENT_PLAYLIST_FRAGMENT_TAG = "current_playlist_fragment_tag";

    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    private String userUid;


    private ListView playlistListView;
    private ArrayList<Playlist> playlists;
    private PlaylistAdapter playlistAdapter;

    // interface to controls playback
    private Playback playback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_playlist, container, false);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        userUid = firebaseAuth.getCurrentUser().getUid();


        // Init interface playback
        playback = (Playback) getActivity();

        playlistListView = (ListView) rootView.findViewById(R.id.playlist_listview);
        playlists = new ArrayList<>();

        playlistAdapter = new PlaylistAdapter(getActivity());

        Query playlistQuery = databaseReference.child("user-playlists/" + userUid);

        playlistQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Playlist playlist = child.getValue(Playlist.class);
                    playlistAdapter.add(playlist);

                    Log.e(TAG, child.getValue().toString());

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        playlistListView.setAdapter(playlistAdapter);
        playlistListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), CurrentPlaylistActivity.class);
                intent.putExtra("UID", playlistAdapter.getItem(position).uid);
                intent.putExtra("NAME", playlistAdapter.getItem(position).name);
                startActivity(intent);
                Log.e(TAG, playlistAdapter.getItem(position).uid);
            }
        });

        return rootView;
    }

}
