package io.github.cfva14.musicapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.cfva14.musicapp.Playback;
import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.activity.AlbumActivity;
import io.github.cfva14.musicapp.activity.ArtistActivity;
import io.github.cfva14.musicapp.adapter.AlbumAdapter;
import io.github.cfva14.musicapp.adapter.ArtistAdapter;
import io.github.cfva14.musicapp.adapter.SearchAdapter;
import io.github.cfva14.musicapp.adapter.TrackAdapter;
import io.github.cfva14.musicapp.custom.ExpandedHeigthGridView;
import io.github.cfva14.musicapp.custom.ExpandedHeigthListView;
import io.github.cfva14.musicapp.model.Album;
import io.github.cfva14.musicapp.model.Artist;
import io.github.cfva14.musicapp.model.Search;
import io.github.cfva14.musicapp.model.Track;

/**
 * Created by Carlos on 3/20/17.
 */

public class SearchFragment extends Fragment {

    private static final String TAG = SearchFragment.class.getSimpleName();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference searchRef = database.getReference("search");
    DatabaseReference trackRef = database.getReference("track");
    DatabaseReference artistRef = database.getReference("artist");
    DatabaseReference albumRef = database.getReference("album");


    @BindView(R.id.search_edittextview) EditText searchEditText;
    @BindView(R.id.search_button) Button searchButton;
    @BindView(R.id.listview_search_results) ListView searchListView;
    @BindView(R.id.search_track_listview) ExpandedHeigthListView trackListView;
    @BindView(R.id.search_artist_gridview) ExpandedHeigthGridView artistGridView;
    @BindView(R.id.search_album_gridview) ExpandedHeigthGridView albumGridView;
    @BindView(R.id.search_searches) RelativeLayout searchesLayout;
    @BindView(R.id.search_result) LinearLayout result;
    @BindView(R.id.search_track_textview) TextView trackTextView;
    @BindView(R.id.search_artist_textview) TextView artistTextView;
    @BindView(R.id.search_album_textview) TextView albumTextView;

    ArrayList<Search> searches;
    ArrayList<Track> tracks;
    ArrayList<Album> albums;
    ArrayList<Artist> artists;

    SearchAdapter searchAdapter;
    TrackAdapter trackAdapter;
    ArtistAdapter artistAdapter;
    AlbumAdapter albumAdapter;

    private String search;

    // interface to controls playback
    private Playback playback;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        // Init interface playback
        playback = (Playback) getActivity();

        // Bind elements from UI
        ButterKnife.bind(this, rootView);

        searches = new ArrayList<>();
        tracks = new ArrayList<>();
        albums = new ArrayList<>();
        artists = new ArrayList<>();

        searchAdapter = new SearchAdapter(getActivity());
        trackAdapter = new TrackAdapter(getActivity());
        artistAdapter = new ArtistAdapter(getActivity());
        albumAdapter = new AlbumAdapter(getActivity());

        searchListView.setAdapter(searchAdapter);
        trackListView.setAdapter(trackAdapter);
        artistGridView.setAdapter(artistAdapter);
        albumGridView.setAdapter(albumAdapter);

        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setSearch(searches.get(position).getSearch());
            }
        });

        Query searchQuery = searchRef.orderByChild("user_id").equalTo("USERID").limitToLast(20);
        searchQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Search search = child.getValue(Search.class);
                    searchAdapter.insert(search, 0);
                    searches.add(search);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSearch(null);
            }
        });

        return rootView;
    }

    private void setUI(){
        trackListView.setAdapter(trackAdapter);
        trackListView.setExpanded(true);
        trackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                playback.setPlaylist(tracks, position);
            }
        });

        albumGridView.setAdapter(albumAdapter);
        albumGridView.setExpanded(true);
        albumGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG, "onItem " + position);
                Intent intent = new Intent(getActivity(), AlbumActivity.class);
                intent.putExtra("ID", albums.get(position).getId());
                intent.putExtra("NAME", albums.get(position).getName());
                intent.putExtra("SOURCE", albums.get(position).getSource());
                Log.e(TAG, "onItem " + position);
                startActivity(intent);
            }
        });

        artistGridView.setAdapter(artistAdapter);
        artistGridView.setExpanded(true);
        artistGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG, "onItem " + position);
                Intent intent = new Intent(getActivity(), ArtistActivity.class);
                intent.putExtra("ID", artists.get(position).getId());
                intent.putExtra("NAME", artists.get(position).getName());
                intent.putExtra("SOURCE", artists.get(position).getSource());
                Log.e(TAG, "onItem " + position);
                startActivity(intent);
            }
        });

        if (trackAdapter.getCount() > 0 || artistAdapter.getCount() > 0 || albumAdapter.getCount() > 0){
            searchesLayout.setVisibility(View.GONE);
            result.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(getActivity(), "No results for your query", Toast.LENGTH_SHORT).show();
        }
    }

    private void setSearch(String value){
        search = searchEditText.getText().toString().toLowerCase();

        if (value != null){
            search = value;
        }

        if (search.equals("")){
            Toast.makeText(getActivity(), "Can't be empty", Toast.LENGTH_SHORT).show();
        } else {

            searchRef.push().setValue(new Search("USERID", search));

            searchAdapter.insert(new Search("USERID", search), 0);
            trackAdapter.clear();
            albumAdapter.clear();
            artistAdapter.clear();

            tracks.clear();
            albums.clear();
            artists.clear();

            Query trackQuery = trackRef.orderByChild("title").startAt(search).endAt(search + "\uf9ff");
            Query albumQuery = albumRef.orderByChild("name").startAt(search).endAt(search + "\uf9ff");
            Query artistQuery = artistRef.orderByChild("name").startAt(search).endAt(search + "\uf9ff");

            trackQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot child : dataSnapshot.getChildren()){
                        Track track = child.getValue(Track.class);
                        trackAdapter.add(track);
                        tracks.add(track);
                    }
                    if (trackAdapter.getCount() > 0){
                        trackTextView.setVisibility(View.VISIBLE);
                    } else {
                        trackTextView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });

            albumQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot child : dataSnapshot.getChildren()){
                        Album album = child.getValue(Album.class);
                        albumAdapter.add(album);
                        albums.add(album);
                    }
                    if (albumAdapter.getCount() > 0){
                        albumTextView.setVisibility(View.VISIBLE);
                    } else {
                        albumTextView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });

            artistQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot child : dataSnapshot.getChildren()){
                        Artist artist = child.getValue(Artist.class);
                        artistAdapter.add(artist);
                        artists.add(artist);
                    }
                    setUI();
                    if (artistAdapter.getCount() > 0){
                        artistTextView.setVisibility(View.VISIBLE);
                    } else {
                        artistTextView.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });

        }
    }



}
