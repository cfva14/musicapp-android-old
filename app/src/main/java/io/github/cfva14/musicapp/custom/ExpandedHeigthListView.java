package io.github.cfva14.musicapp.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

/**
 * Created by Carlos on 3/23/17.
 */

public class ExpandedHeigthListView extends ListView {

    boolean expanded = false;

    public ExpandedHeigthListView(Context context){
        super(context);
    }

    public ExpandedHeigthListView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public ExpandedHeigthListView(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
    }

    public boolean isExpanded(){
        return expanded;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec){

        if (isExpanded()){

            int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);

            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = getMeasuredHeight();

        } else {

            super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        }
    }

    public void setExpanded(boolean expanded){
        this.expanded = expanded;
    }

}
