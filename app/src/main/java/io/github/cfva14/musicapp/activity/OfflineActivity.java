package io.github.cfva14.musicapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import io.github.cfva14.musicapp.R;

public class OfflineActivity extends AppCompatActivity {

    // Logging TAG
    private static final String TAG = OfflineActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);
    }
}
