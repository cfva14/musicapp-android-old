package io.github.cfva14.musicapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.activity.ArtistActivity;
import io.github.cfva14.musicapp.adapter.ArtistAdapter;
import io.github.cfva14.musicapp.model.Artist;

/**
 * Created by Carlos on 3/13/17.
 */

public class BrowseFragment extends Fragment {

    private static final String TAG = BrowseFragment.class.getSimpleName();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("artist");

    private GridView gridView;
    private ArtistAdapter artistAdapter;
    private ArrayList<Artist> artists;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_browse, container, false);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Artist artist = child.getValue(Artist.class);
                    artistAdapter.add(artist);
                    artists.add(artist);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        gridView = (GridView) rootView.findViewById(R.id.browse_gridview);

        artists = new ArrayList<>();
        artistAdapter = new ArtistAdapter(getActivity());

        gridView.setAdapter(artistAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ArtistActivity.class);
                intent.putExtra("ID", artists.get(position).getId());
                intent.putExtra("NAME", artists.get(position).getName());
                intent.putExtra("SOURCE", artists.get(position).getSource());
                startActivity(intent);
            }
        });

        return rootView;
    }

}
