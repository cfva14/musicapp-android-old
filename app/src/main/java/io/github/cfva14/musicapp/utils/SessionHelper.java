package io.github.cfva14.musicapp.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import io.github.cfva14.musicapp.activity.OnlineActivity;
import io.github.cfva14.musicapp.model.Favorite;

/**
 * Created by Carlos on 3/20/17.
 */

public class SessionHelper {

    // Dummy Data
    public static final Boolean FIREBASE_OFFLINE_MODE = false;
    public static final Boolean FIREBASE_WIFI_DOWNLOAD_ONLY = true;
    public static final Boolean FIREBASE_DOWNLOAD_MUSIC_AUTO = true;
    public static final Boolean FIREBASE_WIFI_STREAM_ONLY = false;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    private static final String PREF_NAME = "musicAppPref";

    private static final String KEY_IS_LOGIN = "pref_is_logged_in";
    private static final String KEY_USERNAME = "pref_username";
    private static final String KEY_EMAIL = "pref_email";

    public static final String KEY_OFFLINE_MODE = "pref_offline_mode";
    public static final String KEY_WIFI_DOWNLOAD_ONLY = "pref_wifi_download_only";
    public static final String KEY_DOWNLOAD_MUSIC_AUTO = "pref_download_music_auto";
    public static final String KEY_WIFI_STREAM_ONLY = "pref_wifi_stream_only";

    public SessionHelper(Context context){
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public void createLoginSession(String username, String email){
        editor.putBoolean(KEY_IS_LOGIN, true);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_EMAIL, email);

        editor.putBoolean(KEY_OFFLINE_MODE, FIREBASE_OFFLINE_MODE);
        editor.putBoolean(KEY_WIFI_DOWNLOAD_ONLY, FIREBASE_WIFI_DOWNLOAD_ONLY);
        editor.putBoolean(KEY_DOWNLOAD_MUSIC_AUTO, FIREBASE_DOWNLOAD_MUSIC_AUTO);
        editor.putBoolean(KEY_WIFI_STREAM_ONLY, FIREBASE_WIFI_STREAM_ONLY);

        editor.commit();
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        return user;
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, OnlineActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(i);
    }

    public void getFav(){

    }

}
