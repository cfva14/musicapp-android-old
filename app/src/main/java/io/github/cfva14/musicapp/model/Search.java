package io.github.cfva14.musicapp.model;

/**
 * Created by Carlos on 3/24/17.
 */

public class Search {

    private String user_id;
    private String search;

    public Search(){}

    public Search(String user_id, String search) {
        this.user_id = user_id;
        this.search = search;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
