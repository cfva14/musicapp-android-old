package io.github.cfva14.musicapp.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import io.github.cfva14.musicapp.Playback;
import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.adapter.TileAdapter;
import io.github.cfva14.musicapp.adapter.TrackAdapter;
import io.github.cfva14.musicapp.model.Album;
import io.github.cfva14.musicapp.model.Artist;
import io.github.cfva14.musicapp.model.Track;

/**
 * Created by Carlos on 3/20/17.
 */

public class DiscoverFragment extends Fragment {

    private static final String TAG = DiscoverFragment.class.getSimpleName();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference trackRef = database.getReference("artist");

    private ArrayList<Track> tracks;
    private ArrayList<Album> albums;
    private ArrayList<Artist> artists;

    private ViewPager trackViewPager;
    private ViewPager albumViewPager;
    private ViewPager artistViewPager;

    // interface to controls playback
    private Playback playback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_discover, container, false);

        trackViewPager = (ViewPager) rootView.findViewById(R.id.discover_tracks_viewpager);
        albumViewPager = (ViewPager) rootView.findViewById(R.id.discover_albums_viewpager);
        artistViewPager = (ViewPager) rootView.findViewById(R.id.discover_artists_viewpager);

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

                // Init interface playback
        playback = (Playback) getActivity();

        tracks = new ArrayList<>();
        albums = new ArrayList<>();
        artists = new ArrayList<>();

        trackRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Artist artist = child.getValue(Artist.class);
                    artists.add(artist);
                }

                trackViewPager.setAdapter(new TileAdapter(getActivity(), getChildFragmentManager(), artists));
                albumViewPager.setAdapter(new TileAdapter(getActivity(), getChildFragmentManager(), artists));
                artistViewPager.setAdapter(new TileAdapter(getActivity(), getChildFragmentManager(), artists));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        trackViewPager.getLayoutParams().height = convertDpToPixels(dpHeight, getActivity()) / 2;

        return rootView;
    }

    public static int convertDpToPixels(float dp, Context context){
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }
}
