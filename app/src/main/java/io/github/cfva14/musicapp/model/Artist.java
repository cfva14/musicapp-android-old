package io.github.cfva14.musicapp.model;

import java.util.List;

/**
 * Created by Carlos on 3/22/17.
 */

public class Artist {

    private String id;
    private String name;
    private String source;
    private String display_name;
    private Stat stats;
    private int number_albums;
    private List<String> genre = null;
    private Bio bio;

    public Artist(){}

    public Artist(String id, String name, String source, String display_name, Stat stats, int number_albums, List<String> genre, Bio bio) {
        this.id = id;
        this.name = name;
        this.source = source;
        this.display_name = display_name;
        this.stats = stats;
        this.number_albums = number_albums;
        this.genre = genre;
        this.bio = bio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public Stat getStats() {
        return stats;
    }

    public void setStats(Stat stats) {
        this.stats = stats;
    }

    public int getNumber_albums() {
        return number_albums;
    }

    public void setNumber_albums(int number_albums) {
        this.number_albums = number_albums;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    public Bio getBio() {
        return bio;
    }

    public void setBio(Bio bio) {
        this.bio = bio;
    }
}
