package io.github.cfva14.musicapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.model.Album;
import io.github.cfva14.musicapp.utils.FirebaseHelper;

/**
 * Created by Carlos on 3/22/17.
 */

public class AlbumAdapter extends ArrayAdapter<Album> {
    boolean isFav = false;

    private static final String TAG = AlbumAdapter.class.getSimpleName();

    public AlbumAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.gridview_tile_item, parent, false);
        }

        final Album currentAlbum = getItem(position);

        ImageView imageView = (ImageView) listItemView.findViewById(R.id.gridview_tile_imageview);
        Picasso.with(getContext()).load(currentAlbum.getSource()).into(imageView);

        TextView primaryTextView = (TextView) listItemView.findViewById(R.id.gridview_tile_primary_textview);
        primaryTextView.setText(currentAlbum.getName());
        primaryTextView.setSelected(true);

        TextView secondaryTextView = (TextView) listItemView.findViewById(R.id.gridview_tile_secondary_textview);
        secondaryTextView.setText(currentAlbum.getGenre().toString());
        secondaryTextView.setSelected(true);

        final ImageButton favoriteButton = (ImageButton) listItemView.findViewById(R.id.gridview_tile_favorite_imagebutton);
        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isFav){
                    new FirebaseHelper().removeFav("AAA", currentAlbum.getId());
                    favoriteButton.setBackgroundResource(R.drawable.ic_favorite_border);
                    isFav = false;
                } else {
                    new FirebaseHelper().setFav("AAA", currentAlbum.getId());
                    favoriteButton.setBackgroundResource(R.drawable.ic_favorite_full);
                    isFav = true;
                }

            }
        });

        return listItemView;
    }

}
