package io.github.cfva14.musicapp.model;

import java.util.List;

/**
 * Created by Carlos on 3/22/17.
 */

public class Album {

    private String id;
    private String name;
    private String source;
    private String display_name;
    private String artist_id;
    private String artist_name;
    private List<Integer> genre = null;
    private Stat stats;
    private int duration;
    private String artist;

    public Album(){}

    public Album(String id, String name, String source, String display_name, String artist_id, String artist_name, List<Integer> genre, Stat stats, int duration, String artist) {
        this.id = id;
        this.name = name;
        this.source = source;
        this.display_name = display_name;
        this.artist_id = artist_id;
        this.artist_name = artist_name;
        this.genre = genre;
        this.stats = stats;
        this.duration = duration;
        this.artist = artist;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(String artist_id) {
        this.artist_id = artist_id;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public List<Integer> getGenre() {
        return genre;
    }

    public void setGenre(List<Integer> genre) {
        this.genre = genre;
    }

    public Stat getStats() {
        return stats;
    }

    public void setStats(Stat stats) {
        this.stats = stats;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
}
