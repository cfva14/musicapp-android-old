package io.github.cfva14.musicapp.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.fragment.ArtistFragment;
import io.github.cfva14.musicapp.fragment.ControlsFragment;

public class ArtistActivity extends BaseActivity {

    private static final String TAG = ArtistActivity.class.getSimpleName();

    private static final String ARTIST_FRAGMENT_TAG = "artist_fragment_tag";
    private static final String CONTROLS_FRAGMENT_TAG = "control_fragment_tag";

    private static final String PLAYBACK_STATE = "io.github.cfva14.musicapp.action.PLAYBACK_STATE";

    // States of the mediaplayer
    private int state = 0;
    private static final int STATE_NULL = 0;
    private static final int STATE_BUFFERING = 1;
    private static final int STATE_PLAYING = 2;
    private static final int STATE_PAUSED = 3;
    private static final int STATE_STOPPED = 4;

    public SlidingUpPanelLayout slidingUpPanelLayout;

    private IntentFilter intentFilter;
    private BroadcastReceiver musicStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getIntExtra("PLAYBACK_STATE", 0) == 2){
                Log.e(TAG, "PLAYING");
                // If slidingPanel is hidden Show when Start Playing
                if (slidingUpPanelLayout.getPanelState() == PanelState.HIDDEN){
                    slidingUpPanelLayout.setPanelState(PanelState.COLLAPSED);
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist);

        initializeToolbar();

        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        intentFilter = new IntentFilter("io.github.cfva14.musicapp.action.PLAYBACK_STATE");

        Bundle bundle = new Bundle();
        bundle.putString("ID", getIntent().getStringExtra("ID"));
        bundle.putString("SOURCE", getIntent().getStringExtra("SOURCE"));

        if (getIntent() == null){
            getSupportActionBar().setTitle(R.string.artist_title);
        } else {
            getSupportActionBar().setTitle(getIntent().getStringExtra("NAME"));
        }


        ArtistFragment artistFragment = new ArtistFragment();
        artistFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, artistFragment, ARTIST_FRAGMENT_TAG);
        fragmentTransaction.commit();

        // Open control fragment
        ControlsFragment controlsFragment = new ControlsFragment();
        FragmentTransaction fragmentTransactionControls = getSupportFragmentManager().beginTransaction();
        fragmentTransactionControls.add(R.id.controls_fragment_container, controlsFragment, CONTROLS_FRAGMENT_TAG);
        fragmentTransactionControls.commit();

        // Check the status and show sliding Panel if playing, paused, buffering
        if (bound) {
            Log.e(TAG, musicService.getState() + "");
            if (musicService.getState() == STATE_PLAYING || musicService.getState() == STATE_PAUSED || musicService.getState() == STATE_BUFFERING) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                Log.e(TAG, "SHOW");
            } else {
                Log.e(TAG, "HIDE");
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        }  else {
        Log.e(TAG, "No BOUND");
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
    }

        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Intent intent = new Intent();
                intent.setAction(PLAYBACK_STATE);
                if (newState.equals(SlidingUpPanelLayout.PanelState.COLLAPSED)){
                    intent.putExtra("PLAYBACK_STATE", 5);
                } else if(newState.equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    intent.putExtra("PLAYBACK_STATE", 6);
                }
                sendBroadcast(intent);

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(musicStateReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(musicStateReceiver);
    }
}
