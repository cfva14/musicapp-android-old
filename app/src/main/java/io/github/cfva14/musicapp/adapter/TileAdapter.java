package io.github.cfva14.musicapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;

import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.fragment.ControlsFragment;
import io.github.cfva14.musicapp.fragment.TileFragment;
import io.github.cfva14.musicapp.model.Artist;
import io.github.cfva14.musicapp.model.Track;

/**
 * Created by Carlos on 3/24/17.
 */

public class TileAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private ArrayList<Artist> tiles;

    public TileAdapter(Context context, FragmentManager fragmentManager, ArrayList tiles) {
        super(fragmentManager);
        this.context = context;
        this.tiles = tiles;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString("IMAGE", tiles.get(position).getSource());
        TileFragment tileFragment = new TileFragment();
        tileFragment.setArguments(bundle);
        return tileFragment;
    }

    /**
     * Return the total number of pages.
     */
    @Override
    public int getCount() {
        return tiles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(R.string.app_name);
    }
}