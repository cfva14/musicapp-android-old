package io.github.cfva14.musicapp.fragment;

import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.github.cfva14.musicapp.Playback;
import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.utils.AlbumArtHelper;
import io.github.cfva14.musicapp.utils.BlurHelper;

/**
 * Created by Carlos on 3/20/17.
 */

public class ControlsFragment extends Fragment implements OnSeekBarChangeListener{

    private static final String TAG = ControlsFragment.class.getSimpleName();

    // Actions to determinate if repeat of shuffle is activated
    private static final int ACTION_REPEAT_OFF = 0;
    private static final int ACTION_REPEAT_ONE = 1;
    private static final int ACTION_REPEAT_ALL = 2;

    // States of the mediaplayer
    private int state = 0;
    private static final int STATE_NULL = 0;
    private static final int STATE_BUFFERING = 1;
    private static final int STATE_PLAYING = 2;
    private static final int STATE_PAUSED = 3;
    private static final int STATE_STOPPED = 4;
    private static final int STATE_COLLAPSED = 5;
    private static final int STATE_EXPANDED = 6;

    // Variables to set and check repeat and shuffle modes
    public boolean isShuffle = false;
    public boolean isRepeat = false;
    public boolean isRepeatOne = false;

    // Animation variables
    private ObjectAnimator objectAnimator;

    // interface to controls playback
    private Playback playback;

    // Get elements from UI
    @BindView(R.id.fragment_controls_status_album_imageview) ImageView albumStatusImageView;
    @BindView(R.id.fragment_controls_status_track_title_textview)
    TextView trackTitleStatusTextView;
    @BindView(R.id.fragment_controls_status_artist_name_textview)
    TextView artistNameStatusTextView;
    @BindView(R.id.fragment_controls_status_play_imagebutton) ImageButton playStatusButton;
    @BindView(R.id.fragment_controls_status_playlist_imagebutton) ImageButton playlistStatusButton;
    @BindView(R.id.fragment_controls_status_menu_imagebutton) ImageButton menuStatusButton;


    @BindView(R.id.fragment_controls_background_imageview)
    ImageView backgroundImageView;
    @BindView(R.id.fragment_controls_album_imageview)
    CircleImageView playerCircleImageView;
    @BindView(R.id.fragment_controls_track_title_textview)
    TextView trackTitleTextView;
    @BindView(R.id.fragment_controls_artist_name_textview)
    TextView artistNameTextView;

    @BindView(R.id.fragment_controls_track_progress_seekbar)
    SeekBar trackProgressSeekBar;
    @BindView(R.id.fragment_controls_track_current_time_textview)
    TextView trackCurrentTimeTextView;
    @BindView(R.id.fragment_controls_track_total_time_textview)
    TextView trackTotalTimeTextView;

    @BindView(R.id.fragment_controls_play_imagebutton)
    ImageButton playButton;
    @BindView(R.id.fragment_controls_next_track_imagebutton)
    ImageButton nextButton;
    @BindView(R.id.fragment_controls_previous_track_imagebutton)
    ImageButton previousButton;
    @BindView(R.id.fragment_controls_shuffle_imagebutton)
    ImageButton shuffleButton;
    @BindView(R.id.fragment_controls_repeat_imagebutton)
    ImageButton repeatButton;

    private IntentFilter intentFilter;

    private BroadcastReceiver musicStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getIntExtra("PLAYBACK_STATE", 0)){
                case STATE_NULL:
                    Log.e(TAG, "NULL");
                    break;
                case STATE_BUFFERING:
                    Log.e(TAG, "BUFFERING");
                    setUI();
                    break;
                case STATE_PLAYING:
                    Log.e(TAG, "PLAYING");
                    scheduleSeekbarUpdate();
                    setUI();
                    state = 2;
                    objectAnimator.start();
                    break;
                case STATE_PAUSED:
                    Log.e(TAG, "PAUSED");
                    setUI();
                    break;
                case STATE_STOPPED:
                    Log.e(TAG, "STOPPED");
                    setUI();
                    break;
                case STATE_COLLAPSED:
                    playStatusButton.setVisibility(View.VISIBLE);
                    playlistStatusButton.setVisibility(View.GONE);
                    menuStatusButton.setVisibility(View.GONE);

                    break;
                case STATE_EXPANDED:
                    playStatusButton.setVisibility(View.GONE);
                    playlistStatusButton.setVisibility(View.VISIBLE);
                    menuStatusButton.setVisibility(View.VISIBLE);
                    break;
            }

        }
    };

    private static final long PROGRESS_UPDATE_INTERNAL = 1000;
    private static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;
    private final Handler handler = new Handler();
    private final Runnable updateProgressTask = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> mScheduleFuture;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_controls, container, false);

        intentFilter = new IntentFilter("io.github.cfva14.musicapp.action.PLAYBACK_STATE");
        // Bind elements from UI
        ButterKnife.bind(this, rootView);

        // Init interface playback
        playback = (Playback) getActivity();

        // Set onClickListeners
        setOnClickListeners();

        // If animation is supported, start
        if (Build.VERSION.SDK_INT >= 19) {
            objectAnimator = ObjectAnimator.ofFloat(playerCircleImageView, "rotation", 0, 360);
            objectAnimator.setRepeatCount(ObjectAnimator.INFINITE);
            objectAnimator.setDuration(1000);
            objectAnimator.setInterpolator(new LinearInterpolator());
        }

        playback.getState();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        getActivity().registerReceiver(musicStateReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPaused");
        getActivity().unregisterReceiver(musicStateReceiver);
        stopSeekBarUpdate();
    }

    private void setOnClickListeners() {
        // Set OnClickListeners
        playStatusButton.setOnClickListener(new ClickListener());
        playerCircleImageView.setOnClickListener(new ClickListener());
        playButton.setOnClickListener(new ClickListener());
        nextButton.setOnClickListener(new ClickListener());
        previousButton.setOnClickListener(new ClickListener());
        shuffleButton.setOnClickListener(new ClickListener());
        repeatButton.setOnClickListener(new ClickListener());
        trackProgressSeekBar.setOnSeekBarChangeListener(this);
    }

    class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.fragment_controls_play_imagebutton:
                    Log.d(TAG, "PLAY");
                    playPause();
                    break;
                case R.id.fragment_controls_status_play_imagebutton:
                    Log.d(TAG, "PLAY-FROM_STATUS");
                    playPause();
                    break;
                case R.id.fragment_controls_album_imageview:
                    Log.d(TAG, "PLAY-FROM_SPINNING");
                    playPause();
                    break;
                case R.id.fragment_controls_next_track_imagebutton:
                    Log.d(TAG, "NEXT");
                    animation();
                    playback.nextTrack();
                    break;
                case R.id.fragment_controls_previous_track_imagebutton:
                    Log.d(TAG, "PREVIOUS");
                    animation();
                    playback.previousTrack();
                    break;
                case R.id.fragment_controls_shuffle_imagebutton:
                    setShuffle();
                    break;
                case R.id.fragment_controls_repeat_imagebutton:
                    setRepeat();
                    break;
            }
        }
    }

    private void animation() {
        // Set Animation for API 19 and over
        if (Build.VERSION.SDK_INT >= 19) {
            if (!objectAnimator.isPaused()) {
                objectAnimator.pause();
            } else {
                objectAnimator.resume();
            }
        }
    }

    private void playPause() {
        if (playback.getState() != 0){
            if (state == STATE_PLAYING) {
                state = STATE_PAUSED;

                playButton.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                playStatusButton.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                playback.pause();
                animation();
                //playback.pause();
            } else {
                state = STATE_PLAYING;

                playButton.setImageResource(R.drawable.ic_pause_white_48dp);
                playStatusButton.setImageResource(R.drawable.ic_pause_white_48dp);
                playback.play();
                animation();
            }
        }
    }

    private void setShuffle() {
        if (isShuffle) {
            Log.d(TAG, "REPEAT OFF");
            isShuffle = false;
            shuffleButton.setBackgroundColor(Color.TRANSPARENT);
        } else {
            Log.d(TAG, "REPEAT ON");
            isShuffle = true;
            shuffleButton.setBackgroundResource(R.drawable.circle_button);
        }
    }

    private void setRepeat() {
        if (isRepeat) {
            if (isRepeatOne) {
                Log.d(TAG, "REPEAT OFF");
                isRepeatOne = false;
                isRepeat = false;
                repeatButton.setImageResource(R.drawable.ic_repeat_white_18dp);
                repeatButton.setBackgroundColor(Color.TRANSPARENT);
            } else {
                Log.d(TAG, "REPEAT ONE");
                isRepeatOne = true;
                repeatButton.setImageResource(R.drawable.ic_repeat_one_white_18dp);
            }
        } else {
            Log.d(TAG, "REPEAT ALL");
            isRepeat = true;
            repeatButton.setBackgroundResource(R.drawable.circle_button);
        }
    }

    private void setUI() {

        if(playback.getState() == STATE_PLAYING){
            playButton.setImageResource(R.drawable.ic_pause_white_48dp);
            playStatusButton.setImageResource(R.drawable.ic_pause_white_48dp);
            animation();
        }

        // Info for the bar with the small image
        trackTitleStatusTextView.setText(playback.getTrackTitle());
        artistNameStatusTextView.setText(playback.getArtistName());

        // Info for the main content
        trackTitleTextView.setText(playback.getTrackTitle());
        artistNameTextView.setText(playback.getArtistName());

        // Set SeekBar data
        trackProgressSeekBar.setMax(playback.getTotalDuration());
        trackTotalTimeTextView.setText(DateUtils.formatElapsedTime(playback.getTotalDuration() / 1000));

        String fetchArtAlbumUrl = null;
        String fetchArtArtistUrl = null;
        Bitmap artAlbum = null;
        Bitmap artArtist = null;

        // If current track has images, set image
        if (playback.getAlbumImage() != null) {
            String artAlbumUrl = playback.getAlbumImage();
            artAlbum = AlbumArtHelper.getInstance().getBigImage(artAlbumUrl);

            if (artAlbum == null) {
                fetchArtAlbumUrl = artAlbumUrl;
                artAlbum = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.music_placeholder);
            }
        }

        if (playback.getArtistImage() != null) {
            String artArtistUrl = playback.getArtistImage();
            artArtist = AlbumArtHelper.getInstance().getBigImage(artArtistUrl);
            if (artArtist == null) {
                fetchArtArtistUrl = artArtistUrl;
                artArtist = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.music_placeholder);
            }
        }

        // Set album  images
        albumStatusImageView.setImageBitmap(artAlbum);
        playerCircleImageView.setImageBitmap(artAlbum);

        // set artist background image
        setBackgroundImage(artArtist);

        // If image is not in CACHE, download it
        if (fetchArtAlbumUrl != null) {
            fetchBitmapFromURLAsync(fetchArtAlbumUrl);
        }
        if (fetchArtArtistUrl != null) {
            fetchBitmapFromURLAsync(fetchArtArtistUrl);
        }

    }

    // Download image async
    private void fetchBitmapFromURLAsync(final String bitmapUrl) {
        AlbumArtHelper.getInstance().fetch(bitmapUrl, new AlbumArtHelper.FetchListener() {
            @Override
            public void onFetched(String artUrl, Bitmap bitmap, Bitmap icon) {
                if (playback.getAlbumImage() != null && playback.getAlbumImage().equals(artUrl)) {
                    albumStatusImageView.setImageBitmap(bitmap);
                    playerCircleImageView.setImageBitmap(bitmap);
                }

                if (playback.getArtistImage() != null && playback.getArtistImage().equals(artUrl)) {
                    setBackgroundImage(bitmap);
                }
            }
        });
    }

    private void setBackgroundImage(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 17) {
            // Set blured image as background if available
            Bitmap blurredBitmap = BlurHelper.blur(getActivity(), bitmap, 0.3f, 10.5f);
            backgroundImageView.setImageBitmap(bitmap);
        } else {
            // Set normal image as background
            backgroundImageView.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        trackCurrentTimeTextView.setText(DateUtils.formatElapsedTime(progress / 1000));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        Log.e(TAG, "onStartTrackingTouch");
        stopSeekBarUpdate();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.e(TAG, "onStopTrackingTouch");
        playback.seekTo(seekBar.getProgress());
        scheduleSeekbarUpdate();
    }

    private void updateProgress(){
        if (playback.getState() == 0) {
            return;
        }
        int currentPosition = playback.getCurrentPosition();
        trackProgressSeekBar.setProgress(currentPosition);
    }

    private void scheduleSeekbarUpdate() {
        stopSeekBarUpdate();
        if (!executorService.isShutdown()) {
            mScheduleFuture = executorService.scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            handler.post(updateProgressTask);
                        }
                    }, PROGRESS_UPDATE_INITIAL_INTERVAL,
                    PROGRESS_UPDATE_INTERNAL, TimeUnit.MILLISECONDS);
        }
    }

    private void stopSeekBarUpdate() {
        if (mScheduleFuture != null) {
            mScheduleFuture.cancel(false);
        }
    }

}
