package io.github.cfva14.musicapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.cfva14.musicapp.Playback;
import io.github.cfva14.musicapp.R;
import io.github.cfva14.musicapp.activity.AlbumActivity;
import io.github.cfva14.musicapp.adapter.AlbumAdapter;
import io.github.cfva14.musicapp.adapter.TrackAdapter;
import io.github.cfva14.musicapp.custom.ExpandedHeigthGridView;
import io.github.cfva14.musicapp.custom.ExpandedHeigthListView;
import io.github.cfva14.musicapp.model.Album;
import io.github.cfva14.musicapp.model.Track;


/**
 * Created by Carlos on 3/13/17.
 */

public class AlbumFragment extends Fragment {

    private static final String TAG = AlbumFragment.class.getSimpleName();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference trackRef = database.getReference("track");

    private TrackAdapter trackAdapter;
    private ArrayList<Track> tracks;

    // interface to controls playback
    private Playback playback;

    // Get elements from UI
    @BindView(R.id.fragment_album_cover_imageview) ImageView coverImageView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_album, container, false);

        // Bind elements from UI
        ButterKnife.bind(this, rootView);

        // Init interface playback
        playback = (Playback) getActivity();

        String albumId = getArguments().getString("ID");
        Log.e(TAG, albumId);
        String albumSource = getArguments().getString("SOURCE");

        Picasso.with(getActivity()).load(albumSource).into(coverImageView);

        ExpandedHeigthListView trackListView = (ExpandedHeigthListView) rootView.findViewById(R.id.album_listview);


        trackAdapter = new TrackAdapter(getActivity());

        tracks = new ArrayList<>();

        Query trackQuery = trackRef.orderByChild("album_id").equalTo(albumId);

        trackQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    Track track = child.getValue(Track.class);
                    trackAdapter.add(track);
                    tracks.add(track);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        trackListView.setAdapter(trackAdapter);
        trackListView.setExpanded(true);
        trackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                playback.setPlaylist(tracks, position);
            }
        });

        return rootView;
    }

}
